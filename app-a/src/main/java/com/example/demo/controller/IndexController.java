package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class IndexController {

    private final String message;
    private final String appAMessage;

    public IndexController(
            @Value("${app.message}") String message,
            @Value("${app-a.message:}") String appAMessage) {
        this.message = message;
        this.appAMessage = appAMessage;
    }

    @GetMapping
    public Map<String, String> get() {
        return new HashMap<String, String>() {
            {
                put("message", message);
                put("appAMessage", appAMessage);
            }
        };
    }
}
